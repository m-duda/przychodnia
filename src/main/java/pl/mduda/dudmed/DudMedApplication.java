package pl.mduda.dudmed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DudMedApplication {

	public static void main(String[] args) {
		SpringApplication.run(DudMedApplication.class, args);
	}

}
